# Create a function that takes a list of non-negative integers and strings
# and return a new list without the strings.
# [1, 2, "a", "b"]), [1, 2]

def filter_list(lst):
    return [i for i in lst if type(i) == int]


print(filter_list([1, 2, "a", "b"]))
