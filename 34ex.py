# Create a function that takes a string and returns a string with its
# letters in alphabetical order.
# ("hello"), "ehllo")
def alphabet_soup(txt):
    a = sorted(txt)
    a = ''.join(a)
    return str(a)


print(alphabet_soup("hello"))
