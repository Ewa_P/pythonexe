# Create a function that takes a number as an argument and returns True or False
#  depending on whether the number is symmetrical or not.


# is_symmetrical(1112111) ➞ True


def is_symmetrical(num):
    num = str(num)
    rev = ''
    for i in range(len(num)):
        rev = num[i]+rev
    if num == rev:
        return True
    else:
        return False


print(is_symmetrical(1113411))
