# Create a function that takes a string, checks if it has the same number of
#  "x"s and "o"s and returns either True or False.
#  Return a boolean value (True or False).
# The string can contain any character.
# When no x and no o are in the string, return True.
def XO(txt):
    txt = txt.lower()
    x = 'x'
    o = 'o'
    if x not in txt and o not in txt:
        return True
    elif x in txt and o in txt:
        if txt.count('x') == txt.count('o'):
            return True
        else:
            return False
    elif x or o in txt:
        return False


print(XO('ooxXm'))
