# Create a function that reverses a boolean value and returns the string 
# "boolean expected" if another variable type is given.
def reverse(arg):
    if str(arg) == 'True':
        return False
    elif str(arg) == 'False':
        return True
    else:
        return 'boolean expected'

print(reverse(0))
