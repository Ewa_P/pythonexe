# Create a function that returns the mean of all digits.
# (789), 8)
def mean(num):
    s = 0
    num = str(num)
    for i in num:
        s += int(i)
    return s // len(num)


print(mean(789))
