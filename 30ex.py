# Create a function that takes a list of strings and integers, and filters 
# out the list so that it returns a list of integers only.
# ([1, 2, 3, "a", "b", 4]) ➞ [1, 2, 3, 4]
def filter_list(l):
    return [i for i in l if type(i)==int]

print(filter_list([1, 2, 3, "a", "b", 4]))