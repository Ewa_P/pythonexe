# index_of_caps("eDaBiT") ➞ [1, 3, 5]
def index_of_caps(word):
    l = []
    for i in range(len(word)):
        if word[i].isupper() == True:
            l.append(i)
    return l 
 
print(index_of_caps("eDaBiT"))
