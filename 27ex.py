# Count the amount of ones in the binary representation of an integer.
# So for example, since 12 is '1100' in binary, the return value should be 2.

def count_ones(num):
    num = bin(num)
    return num.count('1')


print(count_ones(12))
