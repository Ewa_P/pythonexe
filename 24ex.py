# Using list comprehensions, create a function that finds all even
# numbers from 1 to the given number.
def find_even_nums(num):
    num = list(range(1, num+1))
    return [x for x in num if x % 2 == 0]


print(find_even_nums(10))
