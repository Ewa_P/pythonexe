# Create a function that sorts a list and removes all duplicate items
# from it.
def setify(lst):
    lst = set(lst)
    return list(sorted(lst))


print(setify([1, 3, 3, 5, 5]))
