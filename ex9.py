# Generate a random number between 1 and 9 (including 1 and 9). 
# Ask the user to guess the number, then tell them whether they guessed 
# too low, too high, or exactly right.
# Keep track of how many guesses the user has taken, 
# and when the game ends, print this out
import random
num=random.randint(0,9)
print(num)
rounds=0

while True:
    com=input('Enter play or quit: ')
    if com == 'quit':
        break
    else:
        rounds = rounds +1
        guess=int(input('Enter the number'))
        if guess==num:
            print('You Guess the number!!')
        elif guess>num:
            print('You to high')
        else:
            print('You too low')

print('you try this game: '+ str(rounds))