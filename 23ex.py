# This Triangular Number Sequence is generated from a pattern of
# dots that form a triangle. The first 5 numbers of the sequence,
#  or dots, are:

# 1, 3, 6, 10, 15

# def triangle(n):
#     dots =0
#     for i in range(n+1):
#         dots+=i
#     return dots
# print(triangle(4))

# recursive
def triangle(n):
    if n == 1:
        return 1
    else:
        return n + triangle(n-1)


print(triangle(8))
