def counterpartCharCode(char):
    if char.isalpha():
        if char.islower():
            char = char.upper()
        else:
            char = char.lower()
    return ord(char)


print(counterpartCharCode('$'))
