import random


def password_generator():
    level = input('weak/strong: ')
    iterator = 0
    password = ''
    if level == 'weak':
        while iterator < 2:
            iterator +=1
            small_letter = random.choice('abcdefghijklmnoprstwuxyz')
            symbol = random.choice('!@#$%^&*()?-+')
            upper_letter = small_letter.upper()
            integer = random.randrange(0, 10)
            password = password+small_letter+symbol+upper_letter+str(integer)
    elif level == 'strong':
        while iterator < 3:
            iterator +=1
            small_letter = random.choice('abcdefghijklmnoprstwuxyz')
            symbol = random.choice('!@#$%^&*()?-+')
            upper_letter = small_letter.upper()
            integer = random.randrange(0, 10)
            password = password+small_letter+symbol+upper_letter+str(integer)
    return password




print(password_generator())
