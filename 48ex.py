# society_name(["Harry", "Newt", "Luna", "Cho"]) ➞ "CHLN"

def society_name(friends):
    name = ''
    friends = sorted(friends)
    for elem in friends:
        elem = str(elem)
        name += elem[0]
    return name
print(society_name(["Harry", "Newt", "Luna", "Cho"]))
