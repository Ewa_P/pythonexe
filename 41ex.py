# Create a function that takes a string
# (will be a person's first and last name) and returns a string with the
#  first and last name swapped.

def name_shuffle(txt):
    x = 0
    for i in range(len(txt)):
        if txt[i] == ' ':
            x = i + 1
            break
    return "'" + txt[x:len(txt)]+' ' + txt[0:x-1] +"'"


print(name_shuffle("Allison Gonzalez"))
