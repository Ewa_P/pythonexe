# Create a function that takes a string and returns the number (count)
# of vowels contained within it.
# ("Portrait"), 3)
def count_vowels(txt):
    count = 0
    for letter in txt:
        if letter in ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']:
            count += 1
    return count


print(count_vowels('Portrait'))
