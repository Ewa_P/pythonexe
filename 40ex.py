# You need to create two functions to substitute str() and int().
#  A function called int_to_str() that converts integers into strings and
#  a function called str_to_int() that converts strings into integers.
def int_to_str(num):
    num = str(num)
    return "'"+ num + "'"
def str_to_int(num):
    return int(num)
print(type(int_to_str(49583908545)))
print(type(str_to_int('49583908545')))