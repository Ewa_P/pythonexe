# Create the function that takes a list of dictionaries and returns the
# sum of people's budgets.
# get_budgets([
#   { "name": "John", "age": 21, "budget": 23000 },
#   { "name": "Steve",  "age": 32, "budget": 40000 },
#   { "name": "Martin",  "age": 16, "budget": 2700 }
# ]) ➞ 65700


def get_budgets(lst):
    budget = 0
    for i in range(len(lst)):
        budget += lst[i]['budget']
    return budget


print(get_budgets([
    {"name": "John", "age": 21, "budget": 23000},
    {"name": "Steve",  "age": 32, "budget": 40000},
    {"name": "Martin",  "age": 16, "budget": 2700}
]))
