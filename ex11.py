# Write a program that takes a list of numbers  and makes a new list of
# only the first and last elements of the given list.
# For practice, write this code inside a function.
# List comprehensions (maybe)
# Functions


def new_list(a=[5, 10, 15, 20, 25]):
    return [a[0], a[-1]]


print(new_list())

a = [5, 10, 15, 20, 25]

b = [i for i in a if i == a[0] or i == a[-1]]

print(b)
