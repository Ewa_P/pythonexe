# missing_num([1, 2, 3, 4, 6, 7, 8, 9, 10]) ➞ 5


def missing_num(lst):
    l = range(1,11)
    for i in l:
        if i not in lst:
            return i


print(missing_num([7, 2, 3, 6, 5, 9, 1, 4, 8]))
