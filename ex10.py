# Ask the user for a number and determine whether the number is
# prime or not
def prime_num():
    num = int(input('Enter the number: '))
    d = []
    for i in range(2, 10):
        if num % i == 0:
            d.append(i)
        print(i)
    if len(d) > 1:
        print('Not Prime')
    else:
        print('Prime')


prime_num()