# Create a function which returns a list of booleans, from a given number.
#  Iterating through the number one digit at a time, append True if the
#  digit is 1 and False if it is 0.
def integer_boolean(n):
    n = list(n)
    return [True if i == '1' else False for i in n]


def integer_boolean_2(n):
    for i in n:
        if i == '1':
            l.append(True)
        else:
            l.append(False)
    return l


print(integer_boolean("100101"))
