# Create a function that takes a string and returns a string in
#  which each character is repeated once.

def double_char(txt):
     a = [ 2*i for i in txt]
     return ''.join(a)


print(double_char("String"))